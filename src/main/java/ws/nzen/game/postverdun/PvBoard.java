/** &copy; beyondRelations, LLC */
package ws.nzen.game.postverdun;

/**  */
public class PvBoard
{
	private PvCell[][] field;
	private int rows = 7, cols = 7;
	private int minesToDeploy = 7;
	//private List<> rulesAndClient;

	/**  */
	public PvBoard()
	{
		startFresh();
	}

	// -- setup

	public void startFresh()
	{
		recreateField();
		layMines();
		calcAdjacent();
	}
	

	private void recreateField()
	{
		field = new PvCell[ rows ][ cols ];
		for ( int indRow = 0; indRow < field.length; indRow++ )
		{
			for ( int indCol = 0; indCol < field[0].length; indCol++ )
			{
				field[ indRow ][ indCol ] = new PvCell();
			}
		}
	}


	private void layMines()
	{
		for ( int mineInd = 0; mineInd < minesToDeploy; mineInd++ )
		{
			field[ 1 ][ 1 ].setHasMine( true );
		}
	}


	private void calcAdjacent()
	{
		for ( int indRow = 0; indRow < field.length; indRow++ )
		{
			for ( int indCol = 0; indCol < field[0].length; indCol++ )
			{
				if ( field[ indRow ][ indCol ].hasMine() )
				{
					// update everything around it
					int minAdjRow = Math.max( 0, indRow -1 );
					int maxAdjRow = Math.min( field.length, indRow +1 );
					int minAdjCol = Math.max( 0, indCol -1 );
					int maxAdjCol = Math.min( field[0].length, indCol +1 );
					for ( int indAdjRow = minAdjRow; indAdjRow < maxAdjRow; indAdjRow++ )
					{
						for ( int indAdjCol = minAdjCol; indAdjCol < maxAdjCol; indAdjCol++ )
						{
							field[ indAdjRow ][ indAdjCol ].incrementNeighbors();
						}
					}
				}
			}
		}
	}


	// -- play
	
	public void uncover( int row, int col )
	{
		if ( row >= 0 && col >= 0
				&& row < field.length && col < field[0].length )
		{
			PvCell currCell = field[ row ][ col ];
			if ( currCell.getShowing() == CellDisplay.covered )
			{
				currCell.setShowing( CellDisplay.uncovered );
			}
		}
	}


	//public GrossGameState


	// FIX
	public String getClientRepresentation()
	{
		/*
		string of numbers?
		object, gson style?
		int[][]?
		
		if numbers, the 0-8 must be for the adjacency
		*/
		String repr = "";
		for ( int indRow = 0; indRow < field.length; indRow++ )
		{
			for ( int indCol = 0; indCol < field[0].length; indCol++ )
			{
				repr += field[ indRow ][ indCol ].display();
			}
			repr += "\n";
		}
		return repr;
	}


}














































