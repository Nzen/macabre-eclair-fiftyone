/** &copy; beyondRelations, LLC */
package ws.nzen.game.postverdun;

/** piece of the board */
public class PvCell
{
	private boolean hasMine = false;
	private CellDisplay showing = CellDisplay.uncovered;
	private int neighbors = 0;

	/**  */
	public PvCell()
	{
		
	}

	public void incrementNeighbors()
	{
		neighbors += 1;
	}

	public String display()
	{
		if ( showing == CellDisplay.covered )
		{
			return "0";
		}
		else if ( showing == CellDisplay.flagged )
		{
			return "P";
		}
		else if ( showing == CellDisplay.marked )
		{
			return "x";
		}
		else // if ( showing == CellDisplay.uncovered )
		{
			if ( hasMine )
			{
				return "X";
			}
			else
			{
				return Integer.toString ( neighbors );
			}
		}
			
	}

	public boolean hasMine()
	{
		return hasMine;
	}

	public void setHasMine( boolean hasMine )
	{
		this.hasMine = hasMine;
	}

	public CellDisplay getShowing()
	{
		return showing;
	}

	public void setShowing( CellDisplay showing )
	{
		this.showing = showing;
	}

	public int getNeighbors()
	{
		return neighbors;
	}

	public void setNeighbors( int neighbors )
	{
		this.neighbors = neighbors;
	}


	

}


















