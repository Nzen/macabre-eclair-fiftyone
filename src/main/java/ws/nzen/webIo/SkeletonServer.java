package ws.nzen.webIo;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONObject;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**  */
public class SkeletonServer extends WebSocketServer
{
	// https://github.com/TooTallNate/Java-WebSocket/blob/master/src/main/example/ChatServer.java

	public static void main( String[] args ) throws UnknownHostException
	{
		SkeletonServer conch = new SkeletonServer();
		conch.start();
	}


	public SkeletonServer() throws UnknownHostException
	{
		super( new InetSocketAddress( 9998 ) );
	}


	@Override
	public void onOpen( WebSocket conn, ClientHandshake handshake )
	{
		System.out.println( "began with "+ conn.getLocalSocketAddress() );
	}


	@Override
	public void onMessage( WebSocket conn, String message )
	{
		System.out.println( "received "+ message +" "+ conn.getLocalSocketAddress() );
		conn.send( echoJsonMsg( message ) );
	}


	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote )
	{
		System.out.println( "finished with "+ conn.getLocalSocketAddress() );
	}


	@Override
	public void onError( WebSocket conn, Exception ex )
	{
		System.err.println( "failed with "+ conn.getLocalSocketAddress() +"\n"+ ex );
	}


	private String echoJsonMsg( String json )
	{
		JSONObject parsedMsg = new JSONObject( json );
		String requestField = "reqType";
		if ( parsedMsg.has( requestField ) )
		{
			String reqType = parsedMsg.getString( requestField );
			if ( reqType.equals( "transform" ) )
			{
				parsedMsg.remove( requestField );
				parsedMsg.append( "ansType", "show" );
				String algorithmField = "isReverse", nameField = "tfName";
				if ( parsedMsg.has( algorithmField )
						&& parsedMsg.has( nameField ) )
				{
					if ( parsedMsg.getBoolean( algorithmField ) )
					{
						parsedMsg.put( nameField, reverse( parsedMsg.getString( nameField ) ) );
					}
					else
					{
						parsedMsg.put( nameField, fakeBase64( parsedMsg.getString( nameField ) ) );
					}
				}
				return parsedMsg.toString();
			}
			else
			{
				return json;
			}
		}
		else
		{
			return json;
		}
	}


	private String reverse( String what )
	{
		if ( what.isEmpty() )
			return what;
		String tahw = "";
		for ( int ind = what.length() -1; ind >= 0; ind-- )
		{
			tahw += what.charAt( ind );
		}
		return tahw;
	}


	private String fakeBase64( String what )
	{
		if ( what.isEmpty() )
			return what;
		String tahw = "";
		for ( int ind = 0; ind < what.length() -1; ind++ )
		{
			tahw += Integer.toString( Character.getNumericValue( what.charAt( ind ) ) );
		}
		return tahw;
	}

}










